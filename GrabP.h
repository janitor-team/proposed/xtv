/*
** GrabP.h for XmtdsGrab widget in .
** 
** Made by MaxTheDogSays (dubray_f@epita.fr && rancur_v@epita.fr
** Login   <vr@epita.fr>
** 
** Started on  Mon May 16 22:12:13 1994 vr
** Last update Tue Nov 29 02:26:04 1994 hydrix
*/

#ifndef _XMTDSGRABP_H_
#define _XMTDSGRABP_H_	1

#include <X11/CoreP.h>
#include "Grab.h"

typedef struct
{
   int				empty;
} XmtdsGrabClassPart;

typedef struct			_XmtdsGrabClassRec
{
   CoreClassPart		core_class;
   XmtdsGrabClassPart		xmtds_grab_class;
} XmtdsGrabClassRec;

extern XmtdsGrabClassRec	xmtdsGrabClassRec;

typedef struct
{
   /* resources */
   String		*display_name;
   int			interval;
   /* private state */
   Display		*display;
   GC			gc;
   XtIntervalId		xt_interval_id;
} XmtdsGrabPart;

typedef struct	_XmtdsGrabRec
{
   CorePart		core;
   XmtdsGrabPart	xmtds_grab;
} XmtdsGrabRec;

#define CORE		(w->core)
#define NEW_CORE	(new->core)

#define GRAB		(w->xmtds_grab)
#define NEW_GRAB	(new->xmtds_grab)

#endif /* _XMTDSGRABP_H_ */

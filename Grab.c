/*
** Grab.c for XmtdsGrab widget in .
**
** Made by MaxTheDogSays (dubray_f@epita.fr && rancur_v@epita.fr
** Login   <vr@epita.fr>
**
** Started on  Mon May 16 22:24:08 1994 vr
** Last update Tue Apr  4 02:31:06 1995 mtds
*/

#include <X11/IntrinsicP.h>
#include <X11/StringDefs.h>
#include <stdio.h>
#include <stdlib.h>
#include "XmtdsI.h"
#include "GrabP.h"

static void			Initialize();
static void			SetValues();
static void			Redisplay();
static void			Destroy();

static void			timer();

static void			copy();

static XtResource	resources[] =
{
{XmtdsNdisplayName,	XmtdsCDisplayName,XtRString,	sizeof (char *),
    XtOffset(XmtdsGrabWidget,	xmtds_grab.display_name),
    XtRString,		NULL},
{XmtdsNinterval,		XmtdsCInterval,	XtRInt,		sizeof (int),
    XtOffset(XmtdsGrabWidget,	xmtds_grab.interval),
    XtRString,		"200"}
};

XmtdsGrabClassRec	xmtdsGrabClassRec = {
{
   /* core */
   (WidgetClass)&widgetClassRec,	/* superclass		*/
   "XmtdsGrab",				/* class name		*/
   sizeof(XmtdsGrabRec),		/* size			*/
   NULL,				/* class initialize	*/
   NULL,				/* class_part_init	*/
   FALSE,				/* class_inited		*/
   Initialize,				/* initialize		*/
   NULL,				/* initialize_hook	*/
   XtInheritRealize,			/* realize		*/
   NULL,				/* actions		*/
   0,					/* num_actions		*/
   resources,				/* resources		*/
   XtNumber(resources),			/* resource_count	*/
   NULLQUARK,				/* xrm_class		*/
   TRUE,				/* compress_motion	*/
   TRUE,				/* compress_exposure	*/
   TRUE,				/* compress_enterleave	*/
   FALSE,				/* visible_interest	*/
   Destroy,				/* destroy		*/
   NULL,				/* resize		*/
   Redisplay,				/* expose		*/
   NULL,				/* set_values		*/
   NULL,				/* set_values_hook	*/
   XtInheritSetValuesAlmost,		/* set_values_almost	*/
   NULL,				/* get_values_hook	*/
   NULL,				/* accept_focus		*/
   XtVersion,				/* version		*/
   NULL,				/* callback_private	*/
   NULL,				/* tm_table		*/
   XtInheritQueryGeometry,		/* query_geometry	*/
   XtInheritDisplayAccelerator,		/* display_accelerator	*/
   NULL,				/* extension		*/
},
/* xmtds_grab */
{
   0,					/* empty		*/
}
};

WidgetClass	xmtdsGrabWidgetClass = (WidgetClass)&xmtdsGrabClassRec;

static void		Initialize(request,new,args,num_args)
XmtdsGrabWidget		request;
XmtdsGrabWidget		new;
ArgList			args;
Cardinal		*num_args;
{
   XGCValues		xgcv;

   if ((NEW_GRAB.display =
	XOpenDisplay((char *)NEW_GRAB.display_name)) == NULL)
   {
      XtWarning("can't open display");
      exit(-1);
   }
   xgcv.function = GXcopy;
   NEW_GRAB.gc = XCreateGC(XtDisplay(new),
			   W_ROOT(new),
			   GCFunction,
			   &xgcv);
   NEW_GRAB.xt_interval_id =
      XtAppAddTimeOut(XtWidgetToApplicationContext((Widget)new),
		      NEW_GRAB.interval,
		      (XtTimerCallbackProc)timer,
		      (void *)new);
   NEW_CORE.width =
      DisplayWidth(NEW_GRAB.display,DefaultScreen(NEW_GRAB.display));
   NEW_CORE.height =
      DisplayHeight(NEW_GRAB.display,DefaultScreen(NEW_GRAB.display));
}

static void		SetValues(old,request,new,args,num_args)
XmtdsGrabWidget		old;
XmtdsGrabWidget		request;
XmtdsGrabWidget		new;
ArgList			args;
Cardinal		*num_args;
{
  /* TODO */
}

static void		Redisplay(w,event,region)
XmtdsGrabWidget		w;
XExposeEvent		*event;
Region			region;
{
   copy(w,event->x,event->y,event->width,event->height);
}

static void		Destroy(w)
XmtdsGrabWidget	w;
{
   XCloseDisplay(GRAB.display);
   XtRemoveTimeOut(GRAB.xt_interval_id);
}

static void			timer(w,id)
XmtdsGrabWidget			w;
XtIntervalId			*id;
{
   Dimension			width;
   Dimension			height;

#ifdef SYNC
   XSync(XtDisplay(w),FALSE);
#endif /* SYNC */
   XtVaGetValues(XtParent(w),
		 XtNwidth,	&width,
		 XtNheight,	&height,
		 NULL);
   copy(w,ABS(CORE.x),ABS(CORE.y),width,height);
   GRAB.xt_interval_id =
      XtAppAddTimeOut(XtWidgetToApplicationContext((Widget)w),
		      GRAB.interval,
		      timer,
		      (void *)w);
}

static void		copy(w,x,y,width,height)
XmtdsGrabWidget		w;
Position		x;
Position		y;
Dimension		width;
Dimension		height;
{
   XImage		*image;
   Dimension		dpy_width;
   Dimension		dpy_height;

   dpy_width = DisplayWidth(GRAB.display,DefaultScreen(GRAB.display));
   dpy_height = DisplayHeight(GRAB.display,DefaultScreen(GRAB.display));
   if ((x + width) > dpy_width)
     width = dpy_width - x;
   if ((y + height) > dpy_height)
     height = dpy_height - y;
   image =
     XGetImage(GRAB.display,
		DefaultRootWindow(GRAB.display),
		x,
		y,
		width,
		height,
		AllPlanes,
		ZPixmap);
   XPutImage(XtDisplay(w),
	     XtWindow(w),
	     GRAB.gc,
	     image,
	     0,
	     0,
	     x,
	     y,
	     width,
	     height);
   XDestroyImage(image);
}

/*
** xtv.c for Mtds in .
** 
** Made by MaxTheDogSays (dubray_f@epita.fr && rancur_v@epita.fr)
** Login   <mtds@epita.fr>
** 
** Started on  Tue May  3 23:29:35 1994 mtds
** Updated on  Tue Apr  4 02:19:53 1995 mtds
** Minor mods  Sun Oct  7 13:42:49 MDT 2001 bap@cs.unm.edu
*/

#include <stdio.h>
#include <stdlib.h>
#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
#include <X11/Xaw/Viewport.h>
#include "Grab.h"

typedef struct	_appl
{
   XtAppContext	app_context;
   Widget	toplevel;
   Widget	grab;
} appl;

static XrmOptionDescRec options[] =
{
{"-d",		"*displayName",
    XrmoptionSepArg,	NULL},
{"-s",		"*interval",
    XrmoptionSepArg,	NULL}
};

void usage()
{
   fprintf(stderr,"usage: xtv [-s refresh_timeout] [-d display]\n");
   exit(255);
}

int main(int argc, char **argv)
{
   appl		app;
   Widget	viewport;
   
   app.toplevel =
      XtVaAppInitialize(&(app.app_context),
			"XTv",
			options,
			XtNumber(options),
			&argc,
			argv,
			NULL,
			NULL);
   if (argc != 1)
      usage();
   viewport =
      XtVaCreateWidget("viewport",
		       viewportWidgetClass,
		       app.toplevel,
		       XtNallowHoriz,	TRUE,
		       XtNallowVert,	TRUE,
		       XtNwidth,	100,
		       XtNheight,	100,
		       NULL);
   app.grab =
      XtVaCreateManagedWidget("grab",
			      xmtdsGrabWidgetClass,
			      viewport,
			      NULL);
   XtManageChild(viewport);
   XtRealizeWidget(app.toplevel);
   XtAppMainLoop(app.app_context);
   return 0;
}
